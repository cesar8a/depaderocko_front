import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueSocialSharing from 'vue-social-sharing'
import VueClipboard from 'vue-clipboard2'
import VCalendar from 'v-calendar'
import 'v-calendar/dist/style.css'


const app = createApp(App)

app.use(router)
app.use(VueSocialSharing)
app.use(VueClipboard)
app.use(VCalendar, {})

app.mount('#app')

// Constantes
//  app.config.globalProperties.back_url = 'http://localhost:8000'; // local
// app.config.globalProperties.back_url = 'https://certiback.eldepaderocko.com'; // test
app.config.globalProperties.back_url = "https://admin.eldepaderocko.com"; // prod

// app.config.globalProperties.img_url = 'http://localhost:8000'; // local
// app.config.globalProperties.img_url = 'https://certiback.eldepaderocko.com'; // test
app.config.globalProperties.img_url = "https://fotos.eldepaderocko.com"; // prod

// app.config.globalProperties.front_url = 'http://localhost:3000'; // local
// app.config.globalProperties.front_url = 'https://certifront.eldepaderocko.com'; // test
app.config.globalProperties.front_url = 'https://eldepaderocko.com'; // prod