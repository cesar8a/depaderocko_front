import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/blog',
      name: 'posts',
      component: () => import('../views/BlogView.vue')
    },
    {
      path: '/post/:slug',
      name: 'post',
      component: () => import('../views/EntradaView.vue')
    },
    {
      path: '/contacto',
      name: 'contacto',
      component: () => import('../views/ContactoView.vue')
    },
    {
      path: '/libro-reclamaciones',
      name: 'reclamaciones',
      component: () => import('../views/ReclamacionesView.vue')
    },
    {
      path: '/registro',
      name: 'registro',
      component: () => import('../views/RegisterView.vue')
    },
    {
      path: '/confirmar-correo/:uuid',
      name: 'confirmar-correo',
      component: () => import('../views/RegisterConfirmView.vue')
    },
    {
      path: '/validar-correo/:uuid',
      name: 'validar-correo',
      component: () => import('../views/ConfirmarCorreoView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/reset-account',
      name: 'reset-account',
      component: () => import('../views/ResetAccountView.vue')
    },
    {
      path: '/pruebas',
      name: 'pruebas',
      component: () => import('../views/PruebasView.vue')
    },
    {
      path: '/registro/adicionales/:uuid',
      name: 'registros-adicionales',
      component: () => import('../views/MyPerfil1View.vue')
    },
    {
      path: '/perfil',
      name: 'perfil',
      component: () => import('../views/PerfilView.vue')
    },
    {
      path: '/nueva-clave/:uuid',
      name: 'nueva-clave',
      component: () => import('../views/NewPasswordView.vue')
    },
    {
      path: '/terminos-condiciones',
      name: 'terminos',
      component: () => import('../views/TerminosCondicionesView.vue')
    },
    {
      path: '/politica-privacidad',
      name: 'politicas',
      component: () => import('../views/PoliticaPrivacidadView.vue')
    },
    {
      path: '/nosotros',
      name: 'nosotros',
      component: () => import('../views/NosotrosView.vue')
    },
    {
      path: '/alquila-tu-propiedad',
      name: 'alquila-tu-propiedad',
      component: () => import('../views/RentPropertyView.vue')
    },
    {
      path: '/edita-tu-propiedad',
      name: 'edita-tu-propiedad',
      component: () => import('../views/EditPropertyView.vue')
    },
    {
      path: '/mis-propiedades', 
      name: 'mis-propiedades',
      component: () => import('../views/MyPropertiesView.vue')
    },
    {
      path: '/mis-propiedades/contacto',
      name: 'mis-propiedades-contacto',
      component: () => import('../views/PersonContactView.vue')
    },
    {
      path: '/mis-propiedades/contacto/natural',
      name: 'mis-propiedades-contacto-natural',
      component: () => import('../views/NaturalPersonContactView.vue')
    },
    {
      path: '/mis-propiedades/contacto/juridica',
      name: 'mis-propiedades-contacto-juridica',
      component: () => import('../views/LegalPersonContactView.vue')
    },
    {
      path: '/mis-propiedades/informacion',
      name: 'mis-propiedades-informacion',
      component: () => import('../views/InformationPropertyView.vue')
    },
    {
      path: '/mis-propiedades/ambientes',
      name: 'mis-propiedades-ambientes',
      component: () => import('../views/AmbientPropertyView.vue')
    },
    {
      path: '/mis-propiedades/videos',
      name: 'mis-propiedades-videos',
      component: () => import('../views/VideoPropertyView.vue')
    },
    {
      path: '/mis-propiedades/disponibilidad',
      name: 'mis-propiedades-disponibilidad',
      component: () => import('../views/AvailabilityPropertyView.vue')
    },
    {
      path: '/mis-propiedades/paquetes',
      name: 'mis-propiedades-paquetes',
      component: () => import('../views/PackagesPropertyView.vue')
    }
  ]
})

export default router
